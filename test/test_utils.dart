// Copyright (c) 2015 Tais P. Hansen. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

library google_oauth2_jwt.jwt.test_utils;

import 'package:asn1lib/asn1lib.dart';
import 'dart:typed_data';
import 'package:cipher/api/rsa.dart';
import 'package:cryptoutils/cryptoutils.dart';
import 'package:bignum/bignum.dart';

// RSA key generation:
// openssl req -x509 -days 365 -newkey rsa:1024 -sha256 -nodes -pubkey -noout \
//     -subj '/C=DK/L=Copenhagen/CN=www.example.com' \
//     -keyout prvkey.pem -out pubkey.pem

final TestPrivateKeyString = '''-----BEGIN PRIVATE KEY-----
MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAO5ZfhBkO9yYdWim
Gss4cEM5uxmkK4LkLTyrMEfmJ28M+tkCq6TikosADdmqpmVuDVeK+IvuyAAAzxre
HPCa98lshEX2mAO1SHrpySxNc4iT/AjtmXgeNF+qsKdYb/Eo7U57/4Y1wYyHGe1e
eXxQB5Th/ZgvwRdHXp8VyWoMwC/JAgMBAAECgYEAhWhd4TrZLq2++aYPVTPv0hDc
Iov7NrYUg+KlkXjHFadEOHcAL7gJofeTyQN1Q0yH9zn3yAsMssPBKB6uQBXdHjIp
c7PTMzlzU6jsb5Z35vjH2TAc3XtHyr3sEOM9BCudZBen+mOovRH3XeowW9rbpxRF
H6M2NZWqJxc2+5GFKwECQQD7Lf2yysRMmH7blqYhakerWSyBqGmKs0Y9rjTzlG5p
DRHsTmCUUXigsLoTlaQojIyoPWhQuLgs7Aj5/mVu4vaRAkEA8ux4GnBA5dttbtOX
8qxTkMeyJMxg8K18Jr2IE0ZZDZwnB/Ou+ldTUcHgBf4Oig3Tpag1SNV5zjKuqi0Q
7IZxuQJBAM1ByQa5Ie6B03C4S+wUgJqqVW9i41Rfqa+v0z+VdSBNeO8FIXEynoOZ
x4VHjI1CRZMaE0ubl43GGJPJ6rPRrrECQQDi9Yeou+QGXSI6/znhoptYkxqfQsv3
UT7DOyTdTUZ6bsAQr911Sw0vP4G8V4bh+NUa2rMd30U0HyoNGJnNQjcRAkA7uKiX
U9mtepr6VcE7kYypHAR4Hble+PcZlzXk7W5t1xzVshRsSu83Dv35Ms6oODJP1XmJ
2EzIdaQBrR4UqJYO
-----END PRIVATE KEY-----
''';

// Create RSAPrivateKey from base64 key.
List _keyLines = TestPrivateKeyString.split('\n').map((String line) => line.trim())
  .where((String line) => line.length > 0).skipWhile((String line) => !line.startsWith('---')).toList();
String _keyBase64 = _keyLines.sublist(1, _keyLines.length - 1).join('');
Uint8List _keyBytes = new Uint8List.fromList(CryptoUtils.base64StringToBytes(_keyBase64));
ASN1Parser _asn1Parser = new ASN1Parser(_keyBytes);
ASN1Sequence _asn1Seq = _asn1Parser.nextObject();
ASN1Sequence _asn1EmbeddedSeq =new ASN1Parser(_asn1Seq.elements[2].valueBytes()).nextObject();
BigInteger _keyModulus = (_asn1EmbeddedSeq.elements[1] as ASN1Integer).valueAsBigInteger;
BigInteger _keyExponent = (_asn1EmbeddedSeq.elements[3] as ASN1Integer).valueAsBigInteger;
BigInteger _keyP = (_asn1EmbeddedSeq.elements[4] as ASN1Integer).valueAsBigInteger;
BigInteger _keyQ = (_asn1EmbeddedSeq.elements[5] as ASN1Integer).valueAsBigInteger;

final TestPrivateKey = new RSAPrivateKey(_keyModulus, _keyExponent, _keyP, _keyQ);

final TestGoogleIdToken = {
  'iat': 1410770387,
  'exp': 1410774287,
  'iss': 'accounts.google.com',
  'sub': '12345678901234567890',
  'aud': '1234567890-youramazingapp.apps.googleusercontent.com',
  'at_hash': 'dxgKHVb2pl9g30gzBN0gog',
  'azp': '1234567890-youramazingapp.apps.googleusercontent.com',
  'c_hash': 'Yk8AmAWG2HFgSvGgTGeU-g',
  'email': 'email@example.com',
  'email_verified': false,
  'hd': 'example.com'
};

final TestSigningInput =
    'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0MTA3NzAzODcsImV4cCI6MTQxMDc3NDI4NywiaXNzIjoiYWNjb3VudHMuZ29vZ2xlLmNvbSIsInN1YiI6IjEyMzQ1Njc4OTAxMjM0NTY3ODkwIiwiYXVkIjpbIjEyMzQ1Njc4OTAteW91cmFtYXppbmdhcHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iXSwiYXRfaGFzaCI6ImR4Z0tIVmIycGw5ZzMwZ3pCTjBnb2ciLCJhenAiOiIxMjM0NTY3ODkwLXlvdXJhbWF6aW5nYXBwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiY19oYXNoIjoiWWs4QW1BV0cySEZnU3ZHZ1RHZVUtZyIsImVtYWlsIjoiZW1haWxAZXhhbXBsZS5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImhkIjoiZXhhbXBsZS5jb20ifQ';

final TestSignature =
    'LaKNdnKHCJnqPNFpay-BWvutl5ratsJNtxrZCwrjhSf_utlq7xhN8ZtA3M7BkaPu60vt0D-nLXy6Le8zQZrnFndj-tQ_XM5cSAm36K1XamB7yjCceOi4CeI-rByun_XP8Jv4yiQDaH5UaZhsIFK0G-05v2fa8j01z1UvFlCcyVs';

final TestJwt = TestSigningInput + '.' + TestSignature;

final TestGoogleAuthResponse = {
  'access_token': 'rYgpM7eIkUcd9e8hg9Fq2j_ai_-zvTjrKv_leEo_ra87TJgwvBWi929ThwZHp',
  'code': 'qxz9kz2IJBLLfRAdvdUxxtIepI3VTMxin9h4hP2cS6MNKA34_XrNbwl_CYGRs1',
  'id_token': TestJwt
};
